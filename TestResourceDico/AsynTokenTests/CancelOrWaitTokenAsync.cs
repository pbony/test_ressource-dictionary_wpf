﻿using System;
using System.Threading.Tasks;

namespace AsynTokenTests
{
    /// <summary>
    /// Represent a token that could be requested for cancel or wait. It is mainly used for pausing or cancelling an activity
    /// </summary>
    public interface ICancelOrWaitTokenAsync
    {
        /// <summary>
        /// This method wil wait one for a Set if any pause has been requested.
        /// If a cancel has been requested, it will throw an Operation cancelled exception
        /// </summary>
        /// <returns></returns>
        Task WaitIfRequestedOrThrowIfCancelledAsync();
    }

    /// <summary>
    /// Represent a control over a class 
    /// </summary>
    public interface ICancelOrWaitTokenControl
    {
        /// <summary>
        /// Returns true if the cwt is paused
        /// </summary>
        bool IsPaused { get; }
        
        /// <summary>
        /// Returns true if the cwt has been cancelled
        /// </summary>
        bool IsCancelled { get; }

        /// <summary>
        /// Request a pause if not paused. If paused, do nothing
        /// </summary>
        void RequestPause();

        /// <summary>
        /// unlock all waiters
        /// </summary>
        void UnlockPause();

        /// <summary>
        /// Request a cancellation. Every component that do a WaitIfRequestedOrThrowIfCancelledAsync
        /// will get an operationCancelledException.
        /// If the cwt is paused, it will also throw an exception for all waiters.
        /// </summary>
        /// <param name="cancellationMessage">the optionnal message that will be used for the cancelled exception</param>
        void RequestCancel(string cancellationMessage = null);
    }

    /// <summary>
    /// Represent two side class:
    ///  - A token to give to time consuming running process for checking if a pause or a cancellation has been requested.
    ///  - A controller to request/unlock pause or cancel the token.
    /// It is mainly used for pausing or cancelling an activity (the activity manager owns the controller and the processed task owns the token)
    /// </summary>
    public sealed class CancelOrWaitTokenAsync : ICancelOrWaitTokenAsync, ICancelOrWaitTokenControl
    {
        private TaskCompletionSource<bool> _tcs;
        private readonly object _key = new object();
        private string _cancellationMessage = "CancelOrWaitTokenAsync has been cancelled";

        public bool IsPaused { get; private set; }
        public bool IsCancelled { get; private set; }
        
        /// <summary>
        /// Create a cancellation or wait token (cwt).
        /// </summary>
        /// <param name="isInitialyPaused">whether or not the cwt is initialy in a paused state.</param>
        public CancelOrWaitTokenAsync(bool isInitialyPaused = false)
        {
            _tcs = new TaskCompletionSource<bool>(TaskCreationOptions.RunContinuationsAsynchronously);
            if (isInitialyPaused)
            {
                IsPaused = true;
            }
            else
            {
                // if the initial state is not paused, set the result immediately
                _tcs.SetResult(true);
            }
        }

        public async Task WaitIfRequestedOrThrowIfCancelledAsync()
        {
            Task currentTask;
            lock (_key)
            {
                // if is cancelled, throw an exception
                if (IsCancelled) throw new OperationCanceledException(_cancellationMessage);
                currentTask = _tcs.Task;
            }
            await currentTask.ConfigureAwait(false);
        }

        public void RequestPause()
        {
            lock (_key)
            {
                // if cancelled, don't do anything
                if (IsCancelled) return;

                // don't do anything if the task is not completed (it means the pause is already requested)
                if (!_tcs.Task.IsCompleted) return;

                // Reset the tcs
                _tcs = new TaskCompletionSource<bool>(TaskCreationOptions.RunContinuationsAsynchronously);
                IsPaused = true;
            }
        }

        public void RequestCancel(string cancellationMessage = null)
        {
            lock (_key)
            {
                // if cancelled, don't do anything (and ignore the message)
                if (IsCancelled) return;
                // else, set the message for future caller if not null and cancel the task by setting an exception:
                IsCancelled = true;
                // Don't forget to set IsPaused to false if the cwt has been paused before cancellation
                IsPaused = false;

                _cancellationMessage = cancellationMessage ?? _cancellationMessage;
                // if the task is completed (not paused) we will rely on IsCancelled field to handle exception for any more caller
                if (_tcs.Task.IsCompleted) return;
                // else we cancel the task
                _tcs.TrySetException(new OperationCanceledException(cancellationMessage));
            }
        }

        public void UnlockPause()
        {
            lock (_key)
            {
                // if cancelled, don't do anything
                if (IsCancelled) return;

                // don't do anything if the task is completed (it means the task is not paused)
                if (_tcs.Task.IsCompleted) return;

                // unlock every waiters by setting the task as completed
                _tcs.SetResult(true);
                IsPaused = false;
            }
        }
    }

}
