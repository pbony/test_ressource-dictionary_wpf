﻿using System;
using System.Threading;
using System.Threading.Tasks;
using NUnit.Framework;

namespace AsynTokenTests
{
    [TestFixture]
    public class CancelOrWaitTokenAsyncTests
    {
        private ICancelOrWaitTokenControl _sutController;
        private ICancelOrWaitTokenAsync _sutToken;
        private int _counter;

        [SetUp]
        public void TestInitialize()
        {
            var cwt = new CancelOrWaitTokenAsync();
            _sutToken = cwt;
            _sutController = cwt;
            _counter = 0;
        }

        [Test]
        public void InitialState()
        {
            //Arrange

            //Act

            //Assert
            Assert.IsFalse(_sutController.IsCancelled);
            Assert.IsFalse(_sutController.IsPaused);
        }

        [Test]
        public void InitialStatePaused()
        {
            //Arrange

            //Act
            var sut = new CancelOrWaitTokenAsync(true);

            //Assert
            Assert.IsFalse(sut.IsCancelled);
            Assert.IsTrue(sut.IsPaused);
        }

        [Test]
        [Timeout(2000)]
        public void IsPaused_State_When_Cancelled()
        {
            //Arrange

            //Act
            _sutController.RequestCancel();

            //Assert
            Assert.IsFalse(_sutController.IsPaused);
        }

        [Test]
        [Timeout(2000)]
        public void IsPaused_State_When_Paused_Then_Cancelled()
        {
            //Arrange

            //Act
            _sutController.RequestPause();
            _sutController.RequestCancel();

            //Assert
            Assert.IsFalse(_sutController.IsPaused);
        }

        [Test]
        [Timeout(2000)]
        public void IsPaused_State_When_Paused()
        {
            //Arrange

            //Act
            _sutController.RequestPause();

            //Assert
            Assert.IsTrue(_sutController.IsPaused);
        }


        [Test]
        [Timeout(2000)]
        public void IsCancelled_State_When_Cancelled()
        {
            //Arrange

            //Act
            _sutController.RequestCancel();

            //Assert
            Assert.IsTrue(_sutController.IsCancelled);
        }

        [Test]
        [Timeout(2000)]
        public void IsCancelled_State_When_Paused()
        {
            //Arrange

            //Act
            _sutController.RequestPause();

            //Assert
            Assert.IsFalse(_sutController.IsCancelled);
        }

        [Test]
        [Timeout(2000)]
        public void Request_Pause_Multiple_Time()
        {
            //Arrange

            //Act
            // should not raise any exception.
            _sutController.RequestPause();
            _sutController.RequestPause();

            //Assert
            Assert.IsTrue(_sutController.IsPaused);
        }

        [Test]
        [Timeout(2000)]
        public void Request_Cancellation_Multiple_Time()
        {
            //Arrange

            //Act
            // should not raise any exception.
            _sutController.RequestCancel();
            _sutController.RequestCancel();

            //Assert
            Assert.IsTrue(_sutController.IsCancelled);
        }

        [Test]
        [Timeout(2000)]
        public async Task Nominal_NoCancellation()
        {
            //Arrange

            //Act
            await Task.Run(async () =>
            {
                // request a wait but no wait nor cancelled so pass
                await _sutToken.WaitIfRequestedOrThrowIfCancelledAsync();
                Interlocked.Increment(ref _counter);
            }).ConfigureAwait(false);

            //Assert
            Assert.AreEqual(1, _counter);
        }

        [Test]
        [Timeout(2000)]
        public async Task Nominal_Cancelled()
        {
            //Arrange
            _sutController.RequestCancel();
            bool hasbeenRaised = false;

            //Act
            try
            {
                await _sutToken.WaitIfRequestedOrThrowIfCancelledAsync();
                Interlocked.Increment(ref _counter);

            }
            catch (OperationCanceledException)
            {
                hasbeenRaised = true;
            }
           
            //Assert
            Assert.IsTrue(hasbeenRaised);
            Assert.AreEqual(0, _counter);
        }

        [Test]
        [Timeout(2000)]
        public async Task Cancelled_With_SpecificMessage()
        {
            //Arrange
            _sutController.RequestCancel("Specific message for cancellation");

            //Act
            bool hasBeenRaised = false;
            try
            {
                await Task.Run(async () =>
                {
                    await _sutToken.WaitIfRequestedOrThrowIfCancelledAsync();
                    Interlocked.Increment(ref _counter);
                });
            }
            catch (OperationCanceledException e)
            {
                Assert.AreEqual("Specific message for cancellation", e.Message);
                hasBeenRaised = true;
            }

            //Assert
            Assert.IsTrue(hasBeenRaised);
        }

        [Test]
        [Timeout(2000)]
        public async Task Nominal_Paused()
        {
            //Arrange
            _sutController.RequestPause();
            var mrev = new ManualResetEventSlim();

            var task = Task.Run(async () =>
            {
                await _sutToken.WaitIfRequestedOrThrowIfCancelledAsync(); // this call will wait an unlock event
                mrev.Set();
            });

            //Act
            var unlock = mrev.Wait(TimeSpan.FromMilliseconds(100)); // NOT GOOD BUT NO ALTERNATIVE KNOWN : WE WAIT THAT THE TASK ABOVE STARTS AND WAIT ONCE but we cannot be sure so we takes some margin (but even, on CI, it could goes wrong)

            //Assert
            Assert.IsFalse(unlock);

            // after test, unlock and wait to avoid cross test issues
            _sutController.UnlockPause();
            await task;
        }

        [Test]
        [Timeout(2000)]
        public async Task InitialStatePaused_Check_Paused()
        {
            //Arrange
            var sut = new CancelOrWaitTokenAsync(true);

            //Act
            var mrev = new ManualResetEventSlim();

            var task = Task.Run(async () =>
            {
                await sut.WaitIfRequestedOrThrowIfCancelledAsync(); // this call will wait an unlock event
                mrev.Set();
            });

            //Act
            var unlock = mrev.Wait(TimeSpan.FromMilliseconds(100)); // NOT GOOD BUT NO ALTERNATIVE KNOWN : WE WAIT THAT THE TASK ABOVE STARTS AND WAIT ONCE but we cannot be sure so we takes some margin (but even, on CI, it could goes wrong)
            
            Assert.IsFalse(unlock);

            // after test, unlock and wait to avoid cross test issues
            sut.UnlockPause();
            await task;
        }

        /// <summary>
        /// When a token is paused THEN cancelled, every waiter should received a taskCancelled exception
        /// </summary>
        [Test]
        [Timeout(2000)]
        public async Task Cancel_While_Paused()
        {
            //Arrange
            bool hasbeenRaised = false;
            _sutController.RequestPause(); // pause before starting
            
            //Act
            _sutController.RequestCancel(); 
            try
            {
                await _sutToken.WaitIfRequestedOrThrowIfCancelledAsync();
                Interlocked.Increment(ref _counter);

            }
            catch (OperationCanceledException)
            {
                hasbeenRaised = true;
            }

            //Assert
            Assert.IsTrue(hasbeenRaised);
            Assert.AreEqual(0, _counter);
        }

        [Test]
        [Timeout(2000)]
        public async Task Multiple_Pause_Call()
        {
            //Arrange
            _sutController.RequestPause(); // pause before starting

            var task = Task.Run(async () =>
            {
                await _sutToken.WaitIfRequestedOrThrowIfCancelledAsync();
                Interlocked.Increment(ref _counter);

                _sutController.RequestPause(); // pause before next call
                await _sutToken.WaitIfRequestedOrThrowIfCancelledAsync();
            });

            //Act
            await Task.Delay(100); // NOT GOOD BUT NO ALTERNATIVE KNOWN : WE WAIT THAT THE TASK ABOVE STARTS AND WAIT ONCE but we cannot be sure so we takes some margin (but even, on CI, it could goes wrong)
            _sutController.UnlockPause();

            // wait again to be sure the task will not complete due to the second call:
            var unlocked = task.Wait(TimeSpan.FromMilliseconds(100));

            //Assert
            Assert.AreEqual(1, _counter);
            Assert.AreEqual(false, unlocked);

            // after test, unlock and wait to avoid cross test issues
            _sutController.UnlockPause();
            await task;
        }


        [Test]
        [Timeout(2000)]
        public async Task Multiple_Request_After_Unlock_Pause()
        {
            //Arrange
            _sutController.RequestPause(); // pause before starting
            _sutController.UnlockPause(); // then immediatly unlock

            //Act
            await Task.Run(async () =>
            {
                for (int i = 0; i < 500; i++)
                {
                    await _sutToken.WaitIfRequestedOrThrowIfCancelledAsync();
                    Interlocked.Increment(ref _counter);
                }
            });

            //Assert
            Assert.AreEqual(500, _counter);
        }

        [Test]
        [Timeout(2000)]
        public async Task Multiple_Pause_Cycle_Call()
        {
            //Arrange
            var task = Task.Run(async () =>
            {
                for (int i = 0; i < 500; i++)
                {
                    _sutController.RequestPause(); // pause before next call to simulate that every call is paused
                    await _sutToken.WaitIfRequestedOrThrowIfCancelledAsync();
                    Interlocked.Increment(ref _counter);
                }
            });

            //Act
            // unlock while the task is not completed
            while (!task.IsCompleted)
            {
                _sutController.UnlockPause();
            }

            //Assert
            Assert.AreEqual(500, _counter);

            // after test, wait to avoid cross test issues
            await task;
        }

        [Test]
        public void RequestPause_After_Cancelled()
        {
            //Arrange
            _sutController.RequestCancel();

            //Act
            _sutController.RequestPause();

            //Assert
            Assert.IsTrue(_sutController.IsCancelled);
            Assert.IsFalse(_sutController.IsPaused);
        }

        [Test]
        public void Unlock_After_Cancelled()
        {
            //Arrange
            _sutController.RequestCancel();

            //Act
            _sutController.UnlockPause();

            //Assert
            Assert.IsTrue(_sutController.IsCancelled);
            Assert.IsFalse(_sutController.IsPaused);
        }
    }
}