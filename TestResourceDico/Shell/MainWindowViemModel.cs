﻿using PRF.Utils.WPF;

namespace Shell
{
    internal sealed class MainWindowViemModel : NotifierBase
    {
        private int _percentageCompleted;

        public int PercentageCompleted
        {
            get => _percentageCompleted;
            set
            {
                if (_percentageCompleted != value) return;
                _percentageCompleted = value;
                Notify();
            }
        }

        public MainWindowViemModel()
        {
            
        }
    }
}