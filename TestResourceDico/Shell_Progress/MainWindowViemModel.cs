﻿using System;
using System.Threading.Tasks;
using AsynTokenTests;
using Castle.DynamicProxy.Generators.Emitters;
using PRF.Utils.WPF;
using PRF.Utils.WPF.Commands;

namespace Shell_Progress
{
    internal sealed class MainWindowViemModel : NotifierBase
    {
        private int _percentageCompleted;

        public DelegateCommandLight StartCommand { get; }
        public DelegateCommandLight CancelCommand { get; }
        public DelegateCommandLight SuspendResumeCommand { get; }

        private CancelOrWaitTokenAsync _currentActivity;
        private string _message;
        private bool _hasStarted;

        public MainWindowViemModel()
        {
            StartCommand = new DelegateCommandLight(ExecuteStartCommand, CanExecuteStartCommand);
            SuspendResumeCommand = new DelegateCommandLight(ExecuteSuspendResumeCommand, CanExecuteSuspendResumeCommand);
            CancelCommand = new DelegateCommandLight(ExecuteCancelCommand, CanExecuteCancelCommand);
        }

        private bool CanExecuteCancelCommand()
        {
            return _currentActivity != null;
        }

        private void ExecuteCancelCommand()
        {
            _currentActivity?.RequestCancel();
        }

        private bool CanExecuteSuspendResumeCommand()
        {
            return _currentActivity != null;
        }

        private void ExecuteSuspendResumeCommand()
        {
            if (_currentActivity.IsPaused)
            {
                _currentActivity.UnlockPause();
            }
            else
            {
                _currentActivity.RequestPause();
            }
        }

        private bool CanExecuteStartCommand()
        {
            return _currentActivity == null;
        }

        private async void ExecuteStartCommand()
        {
            _currentActivity = new CancelOrWaitTokenAsync();
            RaiseCanExecuteChanged();
            try
            {
                for (int i = 0; i < 100; i++)
                {
                    PercentageCompleted = i;
                    await Task.Delay(50).ConfigureAwait(false);
                    var copy = _currentActivity;
                    if (copy != null)
                    {
                        await copy.WaitIfRequestedOrThrowIfCancelledAsync().ConfigureAwait(false);
                    }
                }
                Message = "Work done!";
            }
            catch (Exception e)
            {
                Message = $"task cancelled: {e.Message}";
            }
            finally
            {
                ResetCurrentActivity();
            }
        }

        public int PercentageCompleted
        {
            get => _percentageCompleted;
            set
            {
                if (_percentageCompleted == value) return;
                HasStarted = value > 0;
                _percentageCompleted = value;
                Notify();
            }
        }
        
        public bool HasStarted
        {
            get => _hasStarted;
            set
            {
                if (_hasStarted == value) return;
                _hasStarted = value;
                Notify();
            }
        }

        public string Message
        {
            get => _message;
            set
            {
                if (_message == value) return;
                _message = value;
                Notify();
            }
        }

        private void ResetCurrentActivity()
        {
            _currentActivity = null;
            RaiseCanExecuteChanged();
        }
        
        private void RaiseCanExecuteChanged()
        {
            StartCommand.RaiseCanExecuteChanged();
            SuspendResumeCommand.RaiseCanExecuteChanged();
            CancelCommand.RaiseCanExecuteChanged();
        }
    }
}